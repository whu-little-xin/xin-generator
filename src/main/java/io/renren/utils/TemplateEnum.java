package io.renren.utils;


public enum TemplateEnum {
    DAL(0),
    DAL_2(1),
    CORE(2),
    CORE_2(3),
    WEB(4),
    WEB_2(5),


    ;

    TemplateEnum(Integer type) {
        this.type = type;
    }

    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
